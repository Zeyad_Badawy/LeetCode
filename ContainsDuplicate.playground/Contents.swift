import UIKit

//Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.

func containsDuplicate(_ nums: [Int]) -> Bool {
    
    //MARK:  Brute force approach
    
    //    var tempArray = nums
    //    for _ in nums {
    //        let numberToCompare = tempArray[0]
    //        tempArray.remove(at: 0)
    //        for num in tempArray {
    //            if num == numberToCompare {
    //                return true
    //            }
    //        }
    //    }
    //    return false
    
    //MARK:  Enhanced approach
    
    return Set(nums).count != nums.count
}

var nums = [1,2,3,0]

print(containsDuplicate(nums))
